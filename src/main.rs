use tokio::net::{TcpStream, TcpListener};
use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader};
use tokio::spawn;
use tokio::io::AsyncReadExt;
use std::net::Shutdown;

async fn build_http_response(body: &str) -> String {
    let response_body = format!(
        "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nContent-Length: {}\r\n\r\n{}",
        body.len(),
        body
    );
    response_body
}

async fn handle_client(mut stream: TcpStream) {
    let mut buffer = Vec::new();
    let mut content_length = 0;
    let mut request_complete = false;

    {
        let mut stream_reader = BufReader::new(&mut stream);
        let mut lines = stream_reader.lines();
        while let Ok(Some(line)) = lines.next_line().await {
            println!("{}", line);
            if line.is_empty() {
                request_complete = true;
                break;
            }
            if line.starts_with("Content-Length:") {
                content_length = line.split(": ").nth(1).unwrap().trim().parse::<usize>().unwrap();
            }
        }
    } 

    if request_complete && content_length > 0 {
        let mut stream_reader = BufReader::new(&mut stream);
        buffer.resize(content_length, 0);
        stream_reader.read_exact(&mut buffer).await.unwrap();
        println!("Received body: {}", String::from_utf8_lossy(&buffer));
    }

    if request_complete {
        let response = build_http_response("Rust is a very good language!\n").await;
        stream.write_all(response.as_bytes()).await.unwrap();
        stream.shutdown(Shutdown::Write).unwrap(); 
    }
}

async fn app() {
    let mut listener = TcpListener::bind("127.0.0.1:9999").await.unwrap();
    let mut events_collected = 0;

    loop {
        let (stream, _addr) = listener.accept().await.unwrap();
        spawn(async move {
            handle_client(stream).await;
        });
        events_collected += 1;
        println!("Events Collected {events_collected}");
    }
}

#[tokio::main]
async fn main() {
    app().await;
}
